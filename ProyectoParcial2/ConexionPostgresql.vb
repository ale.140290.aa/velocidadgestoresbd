﻿Imports System.Data.SqlClient
Imports Npgsql

Public Class ConexionPostgresql
    Private conexionBD As New NpgsqlConnection()

    Public Function getConexion() As NpgsqlConnection
        Return conexionBD
    End Function

    Public Sub setConexion(ByVal conexionBD As NpgsqlConnection)
        Me.conexionBD = conexionBD
    End Sub

    Public Function Conectar() As ConexionPostgresql
        Try
            Dim BaseDeDatos As String = "Server=localhost;Port=5432;Database=BDPRODUCTO;UserId=postgres;Password=1234;"
            conexionBD = New NpgsqlConnection(BaseDeDatos)
            conexionBD.Open()
            If conexionBD IsNot Nothing Then
                MsgBox("¡OK Postgresql 10!")
            Else
                MsgBox("Error en la Conexión..")
            End If
        Catch ex As Exception
            MsgBox(ex.Message & "Error en la Conexión..")
        End Try
        Return Me
    End Function

    Public Function Ejecutar(ByVal sql As String) As Boolean
        Try
            Dim sentencia As New NpgsqlCommand(sql, conexionBD)

            sentencia.ExecuteNonQuery()

        Catch ex As NpgsqlException
            MsgBox(ex.Message)
            Return False
        End Try
        Return True
    End Function

End Class
