﻿Imports System.Data.SqlClient

Public Class ConexionSqlserver
    Private conexionBD As SqlConnection
    Public Function getConexion() As SqlConnection
        Return conexionBD
    End Function
    Public Sub setConexion(ByVal conexionBD As SqlConnection)
        Me.conexionBD = conexionBD
    End Sub
    Public Function conectar() As ConexionSqlserver
        Try
            Dim strConexion As String = "Server=ALEASTUDILLO;Database=BDPRODUCTO;integrated security=true"
            conexionBD = New SqlConnection(strConexion)
            conexionBD.Open()
            If conexionBD IsNot Nothing Then
                MsgBox("¡OK SQL Server 2016!")
            Else
                MsgBox("Error en la Conexión..")
            End If
        Catch ex As Exception
            MsgBox("Error en la Conexión.." + ex.Message)
        End Try
        Return Me
    End Function
    Public Function ejecutar(ByVal sql As String) As Boolean
        Try
            Dim cmd As New SqlCommand(sql, conexionBD)
            cmd.ExecuteNonQuery()
        Catch ex As SqlException
            MsgBox(ex.Message)
            Return False
        End Try
        Return True
    End Function

End Class
