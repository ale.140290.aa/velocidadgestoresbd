﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class ConexionMySqlserver
    Private conexionBD As MySqlConnection

    Public Function getConexion() As MySqlConnection
        Return conexionBD
    End Function

    Public Sub setConexion(ByVal conexionBD As MySqlConnection)
        Me.conexionBD = conexionBD
    End Sub

    Public Function conectar() As ConexionMySqlserver
        Try
            Dim BaseDeDatos As String = "Server=localhost;Port=3306;Database=BDPRODUCTO;UserId=root;Password=1234;"
            conexionBD = New MySqlConnection(BaseDeDatos)
            conexionBD.Open()

            If conexionBD IsNot Nothing Then
                MsgBox("¡OK MySQL Server!")
            Else
                MsgBox("Error en la Conexión..")
            End If

        Catch ex As Exception
            MsgBox(ex.Message & "Error en la Conexión..")
        End Try
        Return Me
    End Function

    Public Function ejecutar(ByVal sql As String) As Boolean
        Try
            Dim sentencia As New MySqlCommand(sql, conexionBD)

            sentencia.ExecuteNonQuery()

        Catch ex As MySqlException
            MsgBox(ex.Message)
            Return False
        End Try
        Return True
    End Function
End Class

