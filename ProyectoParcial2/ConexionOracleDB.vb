﻿Imports System.Data.OracleClient
Imports System.Data.SqlClient

Public Class ConexionOracleDB

    Private conexionBD As OracleConnection

    Public Property Conexion() As OracleConnection
        Get
            Return conexionBD
        End Get
        Set(ByVal value As OracleConnection)
            conexionBD = value
        End Set
    End Property

    Public Function Conectar() As ConexionOracleDB
        Try
            Dim BaseDeDatos As String = "Data Source=XE;User Id=ubdproducto;Password=1234567;"
            conexionBD = New OracleConnection(BaseDeDatos)
            conexionBD.Open()

            If conexionBD IsNot Nothing Then
                MsgBox("¡OK oracle 18c!")
            Else
                MsgBox("Error en la Conexión..")
            End If

        Catch ex As Exception
            MsgBox(ex.Message & "Error en la Conexión..")
        End Try
        Return Me
    End Function

    Public Function ejecutar(ByVal sql As String) As Boolean
        Try
            Dim sentencia As New OracleCommand(sql, conexionBD)
            sentencia.ExecuteNonQuery()

        Catch ex As OracleException
            MsgBox(ex.Message)
            Return False
        End Try
        Return True
    End Function

End Class

