﻿Public Class Contador
    Public horaAhora As Integer
    Public minutoAhora As Integer
    Public segundoAhora As Integer
    Public horaDespues As Integer
    Public minutoDespues As Integer
    Public segundoDespues As Integer
    Public tiempo As DateTime

    Public Sub New()
        tiempo = DateTime.Now
        horaAhora = tiempo.Hour
        minutoAhora = tiempo.Minute
        segundoAhora = tiempo.Second
        tiempo = tiempo.AddMinutes(1)
        horaDespues = tiempo.Hour
        minutoDespues = tiempo.Minute
        segundoDespues = tiempo.Second
    End Sub

End Class


