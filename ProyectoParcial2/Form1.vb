﻿
Imports System.Data.SqlClient
Imports Oracle.ManagedDataAccess.Client
Imports System.Data.OracleClient
Imports Npgsql
Imports MySql.Data.MySqlClient
Imports System.Threading
Imports System.Windows.Forms.VisualStyles.VisualStyleElement

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub EstadoOracle_Click(sender As Object, e As EventArgs) Handles EstadoOracle.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim a As New ConexionOracleDB()
                                              a.Conectar()
                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub EstadoPostgres_Click(sender As Object, e As EventArgs) Handles EstadoPostgres.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim a As New ConexionPostgresql()
                                              a.Conectar()
                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub EstadoSql_Click(sender As Object, e As EventArgs) Handles EstadoSql.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim a As New ConexionSqlserver()
                                              a.conectar()

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub EstadoMyql_Click(sender As Object, e As EventArgs) Handles EstadoMyql.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim c As New ConexionMySqlserver()
                                              c.conectar()
                                              c.ejecutar("delete from producto")
                                              MessageBox.Show("Registros borrados de MySQLserver")

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Oraclelmd_Click(sender As Object, e As EventArgs) Handles Oraclelmd.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim a As New Contador()
                                              Dim c As New ConexionOracleDB()
                                              c.Conectar()
                                              Dim contador As Integer = 0

                                              While True
                                                  Dim tiempo As DateTime = DateTime.Now
                                                  Dim hora As Integer = tiempo.Hour
                                                  Dim minuto As Integer = tiempo.Minute
                                                  Dim segundo As Integer = tiempo.Second
                                                  If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                      Exit While
                                                  Else
                                                      c.ejecutar("insert into producto values(" & Convert.ToInt32(Idlabel.Text) & ",'" & Descripcionlabel.Text & "'," & Convert.ToSingle(Costolabel.Text) & "," & Convert.ToSingle(Preciolabel.Text) & ")")
                                                  End If
                                                  contador += 1
                                              End While
                                              OracleDatoslmd.Text = "Se insertaron " & contador & " registros en un minuto"

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Oracleborrar_Click(sender As Object, e As EventArgs) Handles Oracleborrar.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim c As New ConexionOracleDB()
                                              c.Conectar()
                                              c.ejecutar("delete from producto")
                                              MessageBox.Show("Registros borrados de Oracle")

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Oraclesp_Click(sender As Object, e As EventArgs) Handles Oraclesp.Click
        Dim thread As Thread = New Thread(Sub()
                                              Try
                                                  Me.CheckForIllegalCrossThreadCalls = False
                                                  Dim cn As New ConexionOracleDB()
                                                  cn.Conectar()

                                                  Dim a As New Contador()

                                                  Dim cst As OracleClient.OracleCommand = New OracleClient.OracleCommand("PA_INSERTARPRODUCTO", cn.Conexion)
                                                  cst.CommandType = CommandType.StoredProcedure

                                                  Dim contador As Integer = 0

                                                  cst.Parameters.Add("PIDPRODUCTO", OracleDbType.Int32).Value = Idlabel.Text
                                                  cst.Parameters.Add("PDESCRIPCION", OracleDbType.Varchar2).Value = Descripcionlabel.Text
                                                  cst.Parameters.Add("PCOSTO", OracleDbType.Double).Value = Convert.ToDouble(Costolabel.Text)
                                                  cst.Parameters.Add("PPRECIO", OracleDbType.Double).Value = Convert.ToDouble(Preciolabel.Text)



                                                  While True
                                                      Dim tiempo As DateTime = DateTime.Now
                                                      Dim hora As Integer = tiempo.Hour
                                                      Dim minuto As Integer = tiempo.Minute
                                                      Dim segundo As Integer = tiempo.Second
                                                      If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                          Exit While
                                                      Else
                                                          cst.ExecuteNonQuery()
                                                      End If
                                                      contador += 1
                                                  End While

                                                  OracleDatossp.Text = "Se insertaron " & contador & " registros en un minuto"

                                              Catch ex As OracleClient.OracleException
                                                  MsgBox(ex.Message)
                                              End Try
                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Postgrelmd_Click(sender As Object, e As EventArgs) Handles Postgrelmd.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim a As New Contador()
                                              Dim c As New ConexionPostgresql()
                                              c.Conectar()
                                              Dim contador As Integer = 0

                                              While True
                                                  Dim tiempo As DateTime = DateTime.Now
                                                  Dim hora As Integer = tiempo.Hour
                                                  Dim minuto As Integer = tiempo.Minute
                                                  Dim segundo As Integer = tiempo.Second
                                                  If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                      Exit While
                                                  Else
                                                      c.Ejecutar("insert into producto values(" & Convert.ToInt32(Idlabel.Text) & ",'" & Descripcionlabel.Text & "'," & Convert.ToSingle(Costolabel.Text) & "," & Convert.ToSingle(Preciolabel.Text) & ")")
                                                  End If
                                                  contador += 1
                                              End While
                                              PostgreDatoslmd.Text = "Se insertaron " & contador & " registros en un minuto"

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub
    Private Sub Postgreborrar_Click(sender As Object, e As EventArgs) Handles Postgreborrar.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim c As New ConexionPostgresql()
                                              c.Conectar()
                                              c.Ejecutar("delete from producto")
                                              MessageBox.Show("Registros borrados de Postgresql")
                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Postgresp_Click(sender As Object, e As EventArgs) Handles Postgresp.Click
        Dim thread As Thread = New Thread(Sub()
                                              Try
                                                  Me.CheckForIllegalCrossThreadCalls = False
                                                  Dim cn As New ConexionPostgresql()
                                                  cn.Conectar()

                                                  Dim a As New Contador()

                                                  Dim cst As NpgsqlCommand = New NpgsqlCommand("PA_INSERTARPRODUCTO", cn.getConexion())
                                                  cst.CommandType = CommandType.StoredProcedure

                                                  Dim contador As Integer = 0

                                                  cst.Parameters.AddWithValue("@PIDPRODUCTO", Integer.Parse(Idlabel.Text))
                                                  cst.Parameters.AddWithValue("@PDESCRIPCION", Descripcionlabel.Text)
                                                  cst.Parameters.AddWithValue("@PCOSTO", Single.Parse(Costolabel.Text))
                                                  cst.Parameters.AddWithValue("@PPRECIO", Single.Parse(Preciolabel.Text))
                                                  While True
                                                      Dim tiempo As DateTime = DateTime.Now
                                                      Dim hora As Integer = tiempo.Hour
                                                      Dim minuto As Integer = tiempo.Minute
                                                      Dim segundo As Integer = tiempo.Second
                                                      If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                          Exit While
                                                      Else
                                                          cst.ExecuteNonQuery()
                                                      End If
                                                      contador += 1
                                                  End While

                                                  PostgreDatossp.Text = "Se insertaron " & contador & " registros en un minuto"

                                              Catch ex As NpgsqlException
                                                  MsgBox(ex.Message)
                                              End Try

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Sqllmd_Click(sender As Object, e As EventArgs) Handles Sqllmd.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim a As New Contador()
                                              Dim c As New ConexionSqlserver()
                                              c.conectar()
                                              Dim contador As Integer = 0

                                              While True
                                                  Dim tiempo As DateTime = DateTime.Now
                                                  Dim hora As Integer = tiempo.Hour
                                                  Dim minuto As Integer = tiempo.Minute
                                                  Dim segundo As Integer = tiempo.Second
                                                  If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                      Exit While
                                                  Else
                                                      c.ejecutar("insert into producto values(" & Convert.ToInt32(Idlabel.Text) & ",'" & Descripcionlabel.Text & "'," & Convert.ToSingle(Costolabel.Text) & "," & Convert.ToSingle(Preciolabel.Text) & ")")
                                                  End If
                                                  contador += 1

                                              End While
                                              SqlDatoslmd.Text = "Se insertaron " & contador & " registros en un minuto"

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()

    End Sub

    Private Sub Sqlborrar_Click(sender As Object, e As EventArgs) Handles Sqlborrar.Click
        Dim thread As Thread = New Thread(Sub()

                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim ConexionSqlServe As SqlConnection

                                              ConexionSqlServe = New SqlConnection("Server=ALEASTUDILLO;Database=BDPRODUCTO;integrated security=true")
                                              ConexionSqlServe.Open()

                                              Dim ConsultaSQL As String = "TRUNCATE TABLE dbo.producto"

                                              Dim comando As SqlCommand
                                              comando = New SqlCommand(ConsultaSQL, ConexionSqlServe)

                                              comando.ExecuteNonQuery()

                                              MessageBox.Show("Datos Eliminados Correctamente")

                                              ConexionSqlServe.Close()

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Sqlsp_Click(sender As Object, e As EventArgs) Handles Sqlsp.Click
        Dim thread As Thread = New Thread(Sub()
                                              Try
                                                  Me.CheckForIllegalCrossThreadCalls = False
                                                  Dim cn As New ConexionSqlserver()
                                                  cn.conectar()

                                                  Dim a As New Contador()

                                                  Dim cst As SqlCommand = New SqlCommand("PA_INSERTARPRODUCTO", cn.getConexion())
                                                  cst.CommandType = CommandType.StoredProcedure

                                                  Dim contador As Integer = 0

                                                  cst.Parameters.AddWithValue("@PIDPRODUCTO", Idlabel.Text)
                                                  cst.Parameters.AddWithValue("@PDESCRIPCION", Descripcionlabel.Text)
                                                  cst.Parameters.AddWithValue("@PCOSTO", Convert.ToDouble(Costolabel.Text))
                                                  cst.Parameters.AddWithValue("@PPRECIO", Convert.ToDouble(Preciolabel.Text))

                                                  While True
                                                      Dim tiempo As DateTime = DateTime.Now
                                                      Dim hora As Integer = tiempo.Hour
                                                      Dim minuto As Integer = tiempo.Minute
                                                      Dim segundo As Integer = tiempo.Second
                                                      If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                          Exit While
                                                      Else
                                                          cst.ExecuteNonQuery()
                                                      End If
                                                      contador += 1
                                                  End While

                                                  SqlDatossp.Text = "Se insertaron " & contador & " registros en un minuto"

                                              Catch ex As SqlException
                                                  MsgBox(ex.Message)
                                              End Try
                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Mysqllmd_Click(sender As Object, e As EventArgs) Handles Mysqllmd.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim a As New Contador()

                                              Dim c As New ConexionMySqlserver()
                                              c.conectar()
                                              Dim contador As Integer = 0
                                              ' Dim thread As Thread = New Thread(Sub()'
                                              While True
                                                  Dim tiempo As DateTime = DateTime.Now
                                                  Dim hora As Integer = tiempo.Hour
                                                  Dim minuto As Integer = tiempo.Minute
                                                  Dim segundo As Integer = tiempo.Second
                                                  If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                      Exit While
                                                  Else
                                                      c.ejecutar("insert into producto values(" & Convert.ToInt32(Idlabel.Text) & ",'" & Descripcionlabel.Text & "'," & Convert.ToSingle(Costolabel.Text) & "," & Convert.ToSingle(Preciolabel.Text) & ")")

                                                  End If
                                                  contador += 1
                                              End While
                                              MysqlDatoslmd.Text = "Se insertaron " & contador & " registros en un minuto"

                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Mysqlborrar_Click(sender As Object, e As EventArgs) Handles Mysqlborrar.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Dim c As New ConexionMySqlserver()
                                              c.conectar()
                                              c.ejecutar("delete from producto")
                                              MessageBox.Show("Registros borrados de MySqlServer")
                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

    Private Sub Mysqlsp_Click(sender As Object, e As EventArgs) Handles Mysqlsp.Click
        Dim thread As Thread = New Thread(Sub()
                                              Me.CheckForIllegalCrossThreadCalls = False
                                              Try
                                                  Dim cn As New ConexionMySqlserver()
                                                  cn.conectar()

                                                  Dim a As New Contador()

                                                  Dim cst As MySqlCommand = New MySqlCommand("PA_INSERTARPRODUCTO", cn.getConexion())
                                                  cst.CommandType = CommandType.StoredProcedure

                                                  Dim contador As Integer = 0

                                                  cst.Parameters.AddWithValue("@PIDPRODUCTO", Idlabel.Text)
                                                  cst.Parameters.AddWithValue("@PDESCRIPCION", Descripcionlabel.Text)
                                                  cst.Parameters.AddWithValue("@PCOSTO", Convert.ToDouble(Costolabel.Text))
                                                  cst.Parameters.AddWithValue("@PPRECIO", Convert.ToDouble(Preciolabel.Text))

                                                  While True
                                                      Dim tiempo As DateTime = DateTime.Now
                                                      Dim hora As Integer = tiempo.Hour
                                                      Dim minuto As Integer = tiempo.Minute
                                                      Dim segundo As Integer = tiempo.Second
                                                      If hora = a.horaDespues And minuto = a.minutoDespues And segundo = a.segundoDespues Then
                                                          Exit While
                                                      Else
                                                          cst.ExecuteNonQuery()
                                                      End If
                                                      contador += 1
                                                  End While

                                                  MysqlDatossp.Text = "Se insertaron " & contador & " registros en un minuto"

                                              Catch ex As MySqlException
                                                  MsgBox(ex.Message)
                                              End Try
                                          End Sub) With
                                          {
                                          .IsBackground = True
                                          }
        thread.Start()
    End Sub

End Class

