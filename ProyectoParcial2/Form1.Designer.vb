﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.EstadoOracle = New System.Windows.Forms.Button()
        Me.Id = New System.Windows.Forms.Label()
        Me.Descripcion = New System.Windows.Forms.Label()
        Me.Costo = New System.Windows.Forms.Label()
        Me.Idlabel = New System.Windows.Forms.TextBox()
        Me.Descripcionlabel = New System.Windows.Forms.TextBox()
        Me.Precio = New System.Windows.Forms.Label()
        Me.Costolabel = New System.Windows.Forms.TextBox()
        Me.Preciolabel = New System.Windows.Forms.TextBox()
        Me.EstadoPostgres = New System.Windows.Forms.Button()
        Me.EstadoSql = New System.Windows.Forms.Button()
        Me.EstadoMyql = New System.Windows.Forms.Button()
        Me.Oraclelmd = New System.Windows.Forms.Button()
        Me.Oracleborrar = New System.Windows.Forms.Button()
        Me.Oraclesp = New System.Windows.Forms.Button()
        Me.Postgrelmd = New System.Windows.Forms.Button()
        Me.Postgreborrar = New System.Windows.Forms.Button()
        Me.Postgresp = New System.Windows.Forms.Button()
        Me.Sqllmd = New System.Windows.Forms.Button()
        Me.Sqlborrar = New System.Windows.Forms.Button()
        Me.Sqlsp = New System.Windows.Forms.Button()
        Me.Mysqllmd = New System.Windows.Forms.Button()
        Me.Mysqlborrar = New System.Windows.Forms.Button()
        Me.Mysqlsp = New System.Windows.Forms.Button()
        Me.OracleDatoslmd = New System.Windows.Forms.TextBox()
        Me.OracleDatossp = New System.Windows.Forms.TextBox()
        Me.PostgreDatoslmd = New System.Windows.Forms.TextBox()
        Me.PostgreDatossp = New System.Windows.Forms.TextBox()
        Me.SqlDatoslmd = New System.Windows.Forms.TextBox()
        Me.SqlDatossp = New System.Windows.Forms.TextBox()
        Me.MysqlDatoslmd = New System.Windows.Forms.TextBox()
        Me.MysqlDatossp = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'EstadoOracle
        '
        Me.EstadoOracle.Location = New System.Drawing.Point(9, 127)
        Me.EstadoOracle.Margin = New System.Windows.Forms.Padding(2)
        Me.EstadoOracle.Name = "EstadoOracle"
        Me.EstadoOracle.Size = New System.Drawing.Size(154, 28)
        Me.EstadoOracle.TabIndex = 0
        Me.EstadoOracle.Text = "Estado Oracle"
        Me.EstadoOracle.UseVisualStyleBackColor = True
        '
        'Id
        '
        Me.Id.AutoSize = True
        Me.Id.Location = New System.Drawing.Point(50, 15)
        Me.Id.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Id.Name = "Id"
        Me.Id.Size = New System.Drawing.Size(18, 13)
        Me.Id.TabIndex = 1
        Me.Id.Text = "ID"
        '
        'Descripcion
        '
        Me.Descripcion.AutoSize = True
        Me.Descripcion.Location = New System.Drawing.Point(50, 48)
        Me.Descripcion.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Size = New System.Drawing.Size(63, 13)
        Me.Descripcion.TabIndex = 2
        Me.Descripcion.Text = "Descripcion"
        '
        'Costo
        '
        Me.Costo.AutoSize = True
        Me.Costo.Location = New System.Drawing.Point(50, 85)
        Me.Costo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Costo.Name = "Costo"
        Me.Costo.Size = New System.Drawing.Size(34, 13)
        Me.Costo.TabIndex = 3
        Me.Costo.Text = "Costo"
        '
        'Idlabel
        '
        Me.Idlabel.Location = New System.Drawing.Point(116, 15)
        Me.Idlabel.Margin = New System.Windows.Forms.Padding(2)
        Me.Idlabel.Name = "Idlabel"
        Me.Idlabel.Size = New System.Drawing.Size(457, 20)
        Me.Idlabel.TabIndex = 4
        Me.Idlabel.Text = "8976"
        '
        'Descripcionlabel
        '
        Me.Descripcionlabel.Location = New System.Drawing.Point(116, 48)
        Me.Descripcionlabel.Margin = New System.Windows.Forms.Padding(2)
        Me.Descripcionlabel.Name = "Descripcionlabel"
        Me.Descripcionlabel.Size = New System.Drawing.Size(457, 20)
        Me.Descripcionlabel.TabIndex = 5
        Me.Descripcionlabel.Text = "preba"
        '
        'Precio
        '
        Me.Precio.AutoSize = True
        Me.Precio.Location = New System.Drawing.Point(334, 89)
        Me.Precio.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Precio.Name = "Precio"
        Me.Precio.Size = New System.Drawing.Size(37, 13)
        Me.Precio.TabIndex = 6
        Me.Precio.Text = "Precio"
        '
        'Costolabel
        '
        Me.Costolabel.Location = New System.Drawing.Point(116, 83)
        Me.Costolabel.Margin = New System.Windows.Forms.Padding(2)
        Me.Costolabel.Name = "Costolabel"
        Me.Costolabel.Size = New System.Drawing.Size(198, 20)
        Me.Costolabel.TabIndex = 7
        Me.Costolabel.Text = "45"
        '
        'Preciolabel
        '
        Me.Preciolabel.Location = New System.Drawing.Point(388, 89)
        Me.Preciolabel.Margin = New System.Windows.Forms.Padding(2)
        Me.Preciolabel.Name = "Preciolabel"
        Me.Preciolabel.Size = New System.Drawing.Size(185, 20)
        Me.Preciolabel.TabIndex = 8
        Me.Preciolabel.Text = "84"
        '
        'EstadoPostgres
        '
        Me.EstadoPostgres.Location = New System.Drawing.Point(194, 127)
        Me.EstadoPostgres.Margin = New System.Windows.Forms.Padding(2)
        Me.EstadoPostgres.Name = "EstadoPostgres"
        Me.EstadoPostgres.Size = New System.Drawing.Size(155, 28)
        Me.EstadoPostgres.TabIndex = 9
        Me.EstadoPostgres.Text = "Estado Postgresql"
        Me.EstadoPostgres.UseVisualStyleBackColor = True
        '
        'EstadoSql
        '
        Me.EstadoSql.Location = New System.Drawing.Point(380, 127)
        Me.EstadoSql.Margin = New System.Windows.Forms.Padding(2)
        Me.EstadoSql.Name = "EstadoSql"
        Me.EstadoSql.Size = New System.Drawing.Size(169, 28)
        Me.EstadoSql.TabIndex = 10
        Me.EstadoSql.Text = "Estado SQL Server"
        Me.EstadoSql.UseVisualStyleBackColor = True
        '
        'EstadoMyql
        '
        Me.EstadoMyql.Location = New System.Drawing.Point(577, 127)
        Me.EstadoMyql.Margin = New System.Windows.Forms.Padding(2)
        Me.EstadoMyql.Name = "EstadoMyql"
        Me.EstadoMyql.Size = New System.Drawing.Size(160, 28)
        Me.EstadoMyql.TabIndex = 11
        Me.EstadoMyql.Text = "Estado Mysql"
        Me.EstadoMyql.UseVisualStyleBackColor = True
        '
        'Oraclelmd
        '
        Me.Oraclelmd.Location = New System.Drawing.Point(9, 188)
        Me.Oraclelmd.Margin = New System.Windows.Forms.Padding(2)
        Me.Oraclelmd.Name = "Oraclelmd"
        Me.Oraclelmd.Size = New System.Drawing.Size(275, 23)
        Me.Oraclelmd.TabIndex = 12
        Me.Oraclelmd.Text = "Inserciones Oracle LMD"
        Me.Oraclelmd.UseVisualStyleBackColor = True
        '
        'Oracleborrar
        '
        Me.Oracleborrar.Location = New System.Drawing.Point(9, 216)
        Me.Oracleborrar.Margin = New System.Windows.Forms.Padding(2)
        Me.Oracleborrar.Name = "Oracleborrar"
        Me.Oracleborrar.Size = New System.Drawing.Size(275, 23)
        Me.Oracleborrar.TabIndex = 13
        Me.Oracleborrar.Text = "Borrar registros Oracle"
        Me.Oracleborrar.UseVisualStyleBackColor = True
        '
        'Oraclesp
        '
        Me.Oraclesp.Location = New System.Drawing.Point(9, 244)
        Me.Oraclesp.Margin = New System.Windows.Forms.Padding(2)
        Me.Oraclesp.Name = "Oraclesp"
        Me.Oraclesp.Size = New System.Drawing.Size(275, 23)
        Me.Oraclesp.TabIndex = 14
        Me.Oraclesp.Text = "Inserciones Oracle SP"
        Me.Oraclesp.UseVisualStyleBackColor = True
        '
        'Postgrelmd
        '
        Me.Postgrelmd.Location = New System.Drawing.Point(9, 293)
        Me.Postgrelmd.Margin = New System.Windows.Forms.Padding(2)
        Me.Postgrelmd.Name = "Postgrelmd"
        Me.Postgrelmd.Size = New System.Drawing.Size(275, 23)
        Me.Postgrelmd.TabIndex = 15
        Me.Postgrelmd.Text = "Inserciones Postgresql LMD"
        Me.Postgrelmd.UseVisualStyleBackColor = True
        '
        'Postgreborrar
        '
        Me.Postgreborrar.Location = New System.Drawing.Point(9, 321)
        Me.Postgreborrar.Margin = New System.Windows.Forms.Padding(2)
        Me.Postgreborrar.Name = "Postgreborrar"
        Me.Postgreborrar.Size = New System.Drawing.Size(275, 23)
        Me.Postgreborrar.TabIndex = 16
        Me.Postgreborrar.Text = "Borrar registros Postgresql"
        Me.Postgreborrar.UseVisualStyleBackColor = True
        '
        'Postgresp
        '
        Me.Postgresp.Location = New System.Drawing.Point(9, 349)
        Me.Postgresp.Margin = New System.Windows.Forms.Padding(2)
        Me.Postgresp.Name = "Postgresp"
        Me.Postgresp.Size = New System.Drawing.Size(275, 23)
        Me.Postgresp.TabIndex = 17
        Me.Postgresp.Text = "Inserciones Postgres SP"
        Me.Postgresp.UseVisualStyleBackColor = True
        '
        'Sqllmd
        '
        Me.Sqllmd.Location = New System.Drawing.Point(9, 391)
        Me.Sqllmd.Margin = New System.Windows.Forms.Padding(2)
        Me.Sqllmd.Name = "Sqllmd"
        Me.Sqllmd.Size = New System.Drawing.Size(275, 23)
        Me.Sqllmd.TabIndex = 18
        Me.Sqllmd.Text = "Inserciones SQL Server 2016 LMD"
        Me.Sqllmd.UseVisualStyleBackColor = True
        '
        'Sqlborrar
        '
        Me.Sqlborrar.Location = New System.Drawing.Point(9, 418)
        Me.Sqlborrar.Margin = New System.Windows.Forms.Padding(2)
        Me.Sqlborrar.Name = "Sqlborrar"
        Me.Sqlborrar.Size = New System.Drawing.Size(275, 23)
        Me.Sqlborrar.TabIndex = 19
        Me.Sqlborrar.Text = "Borrar registros SQL Server 2016"
        Me.Sqlborrar.UseVisualStyleBackColor = True
        '
        'Sqlsp
        '
        Me.Sqlsp.Location = New System.Drawing.Point(9, 446)
        Me.Sqlsp.Margin = New System.Windows.Forms.Padding(2)
        Me.Sqlsp.Name = "Sqlsp"
        Me.Sqlsp.Size = New System.Drawing.Size(275, 23)
        Me.Sqlsp.TabIndex = 20
        Me.Sqlsp.Text = "Inserciones SQL Server 2016 SP"
        Me.Sqlsp.UseVisualStyleBackColor = True
        '
        'Mysqllmd
        '
        Me.Mysqllmd.Location = New System.Drawing.Point(9, 496)
        Me.Mysqllmd.Margin = New System.Windows.Forms.Padding(2)
        Me.Mysqllmd.Name = "Mysqllmd"
        Me.Mysqllmd.Size = New System.Drawing.Size(275, 23)
        Me.Mysqllmd.TabIndex = 21
        Me.Mysqllmd.Text = "Inserciones MYSQL LMD"
        Me.Mysqllmd.UseVisualStyleBackColor = True
        '
        'Mysqlborrar
        '
        Me.Mysqlborrar.Location = New System.Drawing.Point(9, 524)
        Me.Mysqlborrar.Margin = New System.Windows.Forms.Padding(2)
        Me.Mysqlborrar.Name = "Mysqlborrar"
        Me.Mysqlborrar.Size = New System.Drawing.Size(275, 23)
        Me.Mysqlborrar.TabIndex = 22
        Me.Mysqlborrar.Text = "Borrar registros MYSQL"
        Me.Mysqlborrar.UseVisualStyleBackColor = True
        '
        'Mysqlsp
        '
        Me.Mysqlsp.Location = New System.Drawing.Point(9, 552)
        Me.Mysqlsp.Margin = New System.Windows.Forms.Padding(2)
        Me.Mysqlsp.Name = "Mysqlsp"
        Me.Mysqlsp.Size = New System.Drawing.Size(275, 23)
        Me.Mysqlsp.TabIndex = 23
        Me.Mysqlsp.Text = "Inserciones MYSQL SP"
        Me.Mysqlsp.UseVisualStyleBackColor = True
        '
        'OracleDatoslmd
        '
        Me.OracleDatoslmd.Location = New System.Drawing.Point(289, 191)
        Me.OracleDatoslmd.Margin = New System.Windows.Forms.Padding(2)
        Me.OracleDatoslmd.Name = "OracleDatoslmd"
        Me.OracleDatoslmd.Size = New System.Drawing.Size(457, 20)
        Me.OracleDatoslmd.TabIndex = 28
        '
        'OracleDatossp
        '
        Me.OracleDatossp.Location = New System.Drawing.Point(289, 249)
        Me.OracleDatossp.Margin = New System.Windows.Forms.Padding(2)
        Me.OracleDatossp.Name = "OracleDatossp"
        Me.OracleDatossp.Size = New System.Drawing.Size(457, 20)
        Me.OracleDatossp.TabIndex = 29
        '
        'PostgreDatoslmd
        '
        Me.PostgreDatoslmd.Location = New System.Drawing.Point(289, 296)
        Me.PostgreDatoslmd.Margin = New System.Windows.Forms.Padding(2)
        Me.PostgreDatoslmd.Name = "PostgreDatoslmd"
        Me.PostgreDatoslmd.Size = New System.Drawing.Size(457, 20)
        Me.PostgreDatoslmd.TabIndex = 30
        '
        'PostgreDatossp
        '
        Me.PostgreDatossp.Location = New System.Drawing.Point(289, 353)
        Me.PostgreDatossp.Margin = New System.Windows.Forms.Padding(2)
        Me.PostgreDatossp.Name = "PostgreDatossp"
        Me.PostgreDatossp.Size = New System.Drawing.Size(457, 20)
        Me.PostgreDatossp.TabIndex = 31
        '
        'SqlDatoslmd
        '
        Me.SqlDatoslmd.Location = New System.Drawing.Point(289, 396)
        Me.SqlDatoslmd.Margin = New System.Windows.Forms.Padding(2)
        Me.SqlDatoslmd.Name = "SqlDatoslmd"
        Me.SqlDatoslmd.Size = New System.Drawing.Size(457, 20)
        Me.SqlDatoslmd.TabIndex = 32
        '
        'SqlDatossp
        '
        Me.SqlDatossp.Location = New System.Drawing.Point(289, 451)
        Me.SqlDatossp.Margin = New System.Windows.Forms.Padding(2)
        Me.SqlDatossp.Name = "SqlDatossp"
        Me.SqlDatossp.Size = New System.Drawing.Size(457, 20)
        Me.SqlDatossp.TabIndex = 33
        '
        'MysqlDatoslmd
        '
        Me.MysqlDatoslmd.Location = New System.Drawing.Point(289, 501)
        Me.MysqlDatoslmd.Margin = New System.Windows.Forms.Padding(2)
        Me.MysqlDatoslmd.Name = "MysqlDatoslmd"
        Me.MysqlDatoslmd.Size = New System.Drawing.Size(457, 20)
        Me.MysqlDatoslmd.TabIndex = 34
        '
        'MysqlDatossp
        '
        Me.MysqlDatossp.Location = New System.Drawing.Point(289, 552)
        Me.MysqlDatossp.Margin = New System.Windows.Forms.Padding(2)
        Me.MysqlDatossp.Name = "MysqlDatossp"
        Me.MysqlDatossp.Size = New System.Drawing.Size(457, 20)
        Me.MysqlDatossp.TabIndex = 35
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(753, 596)
        Me.Controls.Add(Me.MysqlDatossp)
        Me.Controls.Add(Me.MysqlDatoslmd)
        Me.Controls.Add(Me.SqlDatossp)
        Me.Controls.Add(Me.SqlDatoslmd)
        Me.Controls.Add(Me.PostgreDatossp)
        Me.Controls.Add(Me.PostgreDatoslmd)
        Me.Controls.Add(Me.OracleDatossp)
        Me.Controls.Add(Me.OracleDatoslmd)
        Me.Controls.Add(Me.Mysqlsp)
        Me.Controls.Add(Me.Mysqlborrar)
        Me.Controls.Add(Me.Mysqllmd)
        Me.Controls.Add(Me.Sqlsp)
        Me.Controls.Add(Me.Sqlborrar)
        Me.Controls.Add(Me.Sqllmd)
        Me.Controls.Add(Me.Postgresp)
        Me.Controls.Add(Me.Postgreborrar)
        Me.Controls.Add(Me.Postgrelmd)
        Me.Controls.Add(Me.Oraclesp)
        Me.Controls.Add(Me.Oracleborrar)
        Me.Controls.Add(Me.Oraclelmd)
        Me.Controls.Add(Me.EstadoMyql)
        Me.Controls.Add(Me.EstadoSql)
        Me.Controls.Add(Me.EstadoPostgres)
        Me.Controls.Add(Me.Preciolabel)
        Me.Controls.Add(Me.Costolabel)
        Me.Controls.Add(Me.Precio)
        Me.Controls.Add(Me.Descripcionlabel)
        Me.Controls.Add(Me.Idlabel)
        Me.Controls.Add(Me.Costo)
        Me.Controls.Add(Me.Descripcion)
        Me.Controls.Add(Me.Id)
        Me.Controls.Add(Me.EstadoOracle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.Text = "Medidor de Velocidad de Gestores BD"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents EstadoOracle As Button
    Friend WithEvents Id As Label
    Friend WithEvents Descripcion As Label
    Friend WithEvents Costo As Label
    Friend WithEvents Idlabel As TextBox
    Friend WithEvents Descripcionlabel As TextBox
    Friend WithEvents Precio As Label
    Friend WithEvents Costolabel As TextBox
    Friend WithEvents Preciolabel As TextBox
    Friend WithEvents EstadoPostgres As Button
    Friend WithEvents EstadoSql As Button
    Friend WithEvents EstadoMyql As Button
    Friend WithEvents Oraclelmd As Button
    Friend WithEvents Oracleborrar As Button
    Friend WithEvents Oraclesp As Button
    Friend WithEvents Postgrelmd As Button
    Friend WithEvents Postgreborrar As Button
    Friend WithEvents Postgresp As Button
    Friend WithEvents Sqllmd As Button
    Friend WithEvents Sqlborrar As Button
    Friend WithEvents Sqlsp As Button
    Friend WithEvents Mysqllmd As Button
    Friend WithEvents Mysqlborrar As Button
    Friend WithEvents Mysqlsp As Button
    Friend WithEvents OracleDatoslmd As TextBox
    Friend WithEvents OracleDatossp As TextBox
    Friend WithEvents PostgreDatoslmd As TextBox
    Friend WithEvents PostgreDatossp As TextBox
    Friend WithEvents SqlDatoslmd As TextBox
    Friend WithEvents SqlDatossp As TextBox
    Friend WithEvents MysqlDatoslmd As TextBox
    Friend WithEvents MysqlDatossp As TextBox
End Class
